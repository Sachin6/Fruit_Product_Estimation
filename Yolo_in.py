# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Yolo_interface.ui'
#
# Created by: PyQt5 UI code generator 5.6
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets 
import sys
from PyQt5.QtGui import QIcon, QPixmap
import numpy as np
import cv2
from PyQt5.QtWidgets import QDialog, QApplication
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QWidget, QLabel
from darkflow.net.build import TFNet
import time
import os.path
class Ui_Dialog(object):
    labelOriginal = None
    labelPreview= None
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1056, 694)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(130, 410, 341, 32))
        
        self.openImages()
        self.openVideoFile()
        self.retranslateUi(Dialog)
        self.creatImageLabelOri()
        self.creatImageLabelPre()

    def creatImageLabelOri(self):
        self.labelOriginal =QtWidgets.QLabel(Dialog) 
        self.labelOriginal.setGeometry(50, 50, 451, 521)

    def creatImageLabelPre(self):
        self.labelPreview =QtWidgets.QLabel(Dialog) 
        self.labelPreview.setGeometry(550, 50, 451, 521) 
        
    def openVideoFile(self):
        self.startCam = QtWidgets.QPushButton(Dialog)
        self.startCam.setGeometry(QtCore.QRect(170, 600, 100, 40))
        self.startCam.setObjectName("Select Video")
        self.startCam.clicked.connect(self.show_webcam)

    def show_webcam(mirror=True):

        videoPath = QtWidgets.QFileDialog.getOpenFileName(None,'OpenFile','', "*.avi") 
        print(videoPath)
        cam = cv2.VideoCapture(videoPath)  
        while True:
            ret_val, img = cam.read()
            if mirror: 
                imgVideo = cv2.flip(img, 1)
                options = {
                    'model': 'cfg/tiny-yolo-voc-1c.cfg',
                    'load': 4500,
                    'threshold': 0.2,
                }
                tfnet = TFNet(options)
                colors = [tuple(255 * np.random.rand(3)) for _ in range(10)]
                results = tfnet.return_predict(imgVideo)
                if True:
                    for color, result in zip(colors, results):
                        tl = (result['topleft']['x'], result['topleft']['y'])
                        br = (result['bottomright']['x'], result['bottomright']['y'])
                        label = result['label']

                        conLevel=result['confidence']*100 
                        print(result['label']+ ": " +str( int(conLevel))+"%")
                        confidence = result['confidence']

                        text = '{}: {:.0f}%'.format(label, confidence * 100)
                        testImg = cv2.rectangle(imgVideo, tl, br, color, 2)
                        testImg = cv2.putText(imgVideo, text, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (73, 233, 32), 2)
                testImg=cv2.imshow('ImageWindow', imgVideo)
            if cv2.waitKey(1) == 27: 
                break  # esc to quit
        cv2.destroyAllWindows()

    def openImages(self):
        self.openImg = QtWidgets.QPushButton(Dialog)
        self.openImg.setGeometry(QtCore.QRect(60, 600, 100, 40))
        self.openImg.setObjectName("openImg")
        self.openImg.clicked.connect(self.browseImage)

    def browseImage(self): 
         
        image = QtWidgets.QFileDialog.getOpenFileName(None,'OpenFile','', "*.jpg") 
        imagePath = image[0] 
        self.showOriginal(imagePath)
        self.runYOLO(imagePath)
    # global label

    def showOriginal(self,imagePath):
        
        pixmap = QtGui.QPixmap(imagePath)
        self.labelOriginal.setPixmap(pixmap) 
        self.labelOriginal.show()

    def showPreview(self,image):
        pixmap = QtGui.QPixmap(image) 
        self.labelPreview.setPixmap(pixmap) 
        self.labelPreview.show()
        
    def runYOLO(self,imagePath):

        self.runModel = QtWidgets.QPushButton(Dialog)
        self.runModel.setGeometry(QtCore.QRect(380, 110, 89, 25))
        self.runModel.setObjectName("Run")
        
        save_path = '/home/liveroom'
        options = {
            'model': 'cfg/tiny-yolo-voc-1c.cfg',
            'load': 4500,
            'threshold': 0.2,
        }
        tfnet = TFNet(options)
        colors = [tuple(255 * np.random.rand(3)) for _ in range(10)]
        imageDir = cv2.imread(imagePath)
        results = tfnet.return_predict(imageDir)
        if True:
            x=0
            for color, result in zip(colors, results):
                tl = (result['topleft']['x'], result['topleft']['y'])
                br = (result['bottomright']['x'], result['bottomright']['y'])
                label = result['label']
                
                x+=1
              
                conLevel=result['confidence']*100 
                print(result['label']+ ": " +str( int(conLevel))+"%")
                confidence = result['confidence']
                text = '{}: {:.0f}%'.format(label, confidence * 100)
                imageDir = cv2.rectangle(imageDir, tl, br, color, 2)
                imageDir = cv2.putText(imageDir, text, tl, cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 0), 2)
            imgOut=cv2.imwrite('sample.jpg',imageDir)
            self.showPreview('sample.jpg')
            
           

            print((abs(50 - x) / x) * 100.0)


    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate

        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.startCam.setText(_translate("Dialog", "Select Video"))
        self.openImg.setText(_translate("Dialog", "Open Image"))

if __name__ == "__main__":
  
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    # MainWindow = GUI_fsa()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

